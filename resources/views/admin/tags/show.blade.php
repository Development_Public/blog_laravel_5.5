@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Visualizar Tag
                </div>

                <div class="panel-body">
                    <p><strong>Nome</strong> {{ $tag->name }}</p>
                    <p><strong>URL Amigavel</strong> {{ $tag->slug }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
